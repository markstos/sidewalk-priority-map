// This was used on the 2018 Pavement Center Centerline GeoJSON file.
// Ref: https://data.bloomington.in.gov/dataset/2018-pavement-condition-report
import fs from 'fs';
import { featureEach } from '@turf/meta';

const inFile = fs.readFileSync('/dev/stdin').toString();
const inJSON = JSON.parse(inFile);

// reduce file size by simplifying the metadata to only name, width and speed.

featureEach(inJSON, (feature) => {
  feature.properties = {
    AVG_WIDTH: feature.properties.AVG_WIDTH,
    speed: feature.properties.speed,
    fullname: feature.properties.fullname,
  };
});

fs.writeFileSync('/dev/stdout', JSON.stringify(inJSON));
process.exit();
