/*

Given a hex grid, Read a harcoded GeoJSON file containing Ponts for locations
that have been traffic-calmed.

For each hex, I'll count the number of traffic calming points it contain. Let's
say the resulting range will be 0-7 traffic calmed points.

Convert this range to 100-0. That is, a score of zero (no calming)
would get the highest score for sidewalk priority, while the location with 7
traffic calmed spots would score a zero for sidewalk priority.

*/
import _ from 'lodash';
import fs from 'fs';
import { featureEach } from '@turf/meta';
// Takes a Point and Polygon or MultiPolygon and determines if the point resides inside the polygon.
import boolPointInPolygon from '@turf/boolean-point-in-polygon';
import { getCoords } from '@turf/invariant';
// scale(number, oldMin, oldMax, newMin, newMax);
import scale from 'scale-number-range';

// Read hex file input from STDIN. Start with hex-file-sample.geojson first!
const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

// Load the Traffic-calmed locations data.
// It's an FeatureCollection full of Points.
const calmedFile = fs.readFileSync('TrafficCalmed-locations-in-Bloomington-2021.geojson');
// FC = Feature Collection
const calmPointsFC = JSON.parse(calmedFile);

// For each hex,
featureEach(hexFC, (hexCell) => {
  // shorthand
  const props = hexCell.properties;

  // reset any pre-existing detected traffic
  delete props.trafficCalmingScore;
  props.calmPointsCnt = 0;

  featureEach(calmPointsFC, (calmPoint) => {
    // If the calmed point is within the hex, increments the count for that hex.
    if (boolPointInPolygon(getCoords(calmPoint), hexCell)) {
      props.calmPointsCnt += 1;
    }
  });
});

// Calculate the minimum and maximum traffic calmed points per hex.
// A min of zero is presumed, but we confirm anyway.
const minCnt = _.min(_.map(hexFC.features, 'properties.calmPointsCnt'));
const maxCnt = _.max(_.map(hexFC.features, 'properties.calmPointsCnt'));

// For each hex, calculate the trafficCalmedScore
featureEach(hexFC, (hexCell) => {
  // shorthand
  const props = hexCell.properties;

  if (props.calmPointsCnt === 0) {
    props.trafficCalmedScore = 100;
  } else {
    // Scale the count from 100 to 0, so that locations with less calming score higher.
    props.trafficCalmedScore = scale(props.calmPointsCnt, minCnt, maxCnt, 100, 0);
  }
});

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
