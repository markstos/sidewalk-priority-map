/*

Given a hex grid, Read a missing sidewalk line file
and calculate the distance in KM of missing sidewalks within each hex.

Safe to re-run because we reset the scores of each hex when wen run.

The missing sidewalk file is a feature collection of MultiLineString.

*/
import fs from 'fs';
import { featureEach, flattenEach } from '@turf/meta';
import boolIntersects from '@turf/boolean-intersects';
// Return true if first geometry is completely within the second.
import boolWithin from '@turf/boolean-within';
import center from '@turf/center';
import length from '@turf/length';
// Given a LineString and a feature, return a feature collection of line strings.
import lineSplit from '@turf/line-split';

// Read hex file input from STDIN. Start with hex-file-sample.geojson first!
const hexFile = fs.readFileSync('/dev/stdin').toString();

const hexFC = JSON.parse(hexFile);

// Load the Missing Sidewalk data. It's an FeatureCollection full of MultiLineString.
const sidewalksFile = fs.readFileSync('2021-08-07-missing-sidewalks-2.geojson');
// FC = Feature Collection
const sidewalkFC = JSON.parse(sidewalksFile);

// 5 miles is about 8 kilometers
// 10 miles is aobut 16 kilometers
// Sort the hexes by a value from high to low. Later this will be the final formula.
// As a proof-of-concept, we are just using WalkPotential
// For each hex
const fiveMileKm = 8;
const tenMileKm = 16;

let distanceSoFar = 0;

// Pre-sort the hexes so we can calcuate top miles of missing segments in one pace.
// We sort high to low, so sort order is reversed.
// TODO: For final version, compare based on the final formula.
hexFC.features.sort(
  (first, second) => second.properties.walkPotential - first.properties.walkPotential,
);

// For each hex,
featureEach(hexFC, (hexCell) => {
  // Just a shorthand
  const props = hexCell.properties;

  // Initialize every cell so it's safe to re-run on the same file.
  props.missingSidewalkKm = 0;
  delete props.topTenMiles;
  delete props.topFiveMiles;

  // iterate of the MultiLineStrings flattened to LineStrings
  flattenEach(sidewalkFC, (sidewalk) => {
    // if the hex completely contains the missing sidewalk
    if (boolWithin(sidewalk, hexCell)) {
      props.missingSidewalkKm += length(sidewalk);

    // If the missing sidewalk intersects the hex.
    } else if (boolIntersects(hexCell, sidewalk)) {
      // The sidewalk may be MultiLineString. Convert to a lineString and split
      const segmentsFC = lineSplit(sidewalk, hexCell);
      // For each segment, calculate the center and check see the center is within the hex.
      featureEach(segmentsFC, (segment) => {
        if (boolWithin(center(segment), hexCell)) {
          props.missingSidewalkKm += length(segment);
        }
      });
    }
  });

  distanceSoFar += props.missingSidewalkKm;

  if (props.missingSidewalkKm && (distanceSoFar < tenMileKm)) {
    props.topTenMiles = true;
  }
  if (props.missingSidewalkKm && (distanceSoFar < fiveMileKm)) {
    props.topFiveMiles = true;
  }
});

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
