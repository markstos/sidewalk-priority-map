/*

Given a hex grid, Read a GeoJSON file containing road centerlines with AADT values
and calculate the max AADT for each by hex looking at streets which intersect each hex.

Safe to re-run because we reset the scores of each hex when wen run.

The AADT file is a feature collection of LineStrings.

The properties we are looking for are named `AVG_WIDTH` and `speed`.

*/
import _ from 'lodash';
import fs from 'fs';
import { featureEach } from '@turf/meta';
import boolIntersects from '@turf/boolean-intersects';
// scale(number, oldMin, oldMax, newMin, newMax);
import scale from 'scale-number-range';

// Read hex file input from STDIN. Start with hex-file-sample.geojson first!
const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

// Load the AADT data. It's an FeatureCollection full of LineStrings.
const aadtFile = fs.readFileSync('shapefiles/AADT-2020-clipped-to-bloomington.geojson');
// FC = Feature Collection
const aadtFC = JSON.parse(aadtFile);

// Find the min and max AADTs of roads in Bloomington
const minAADT = _.min(_.map(aadtFC.features, 'properties.AADT'));
const maxAADT = _.max(_.map(aadtFC.features, 'properties.AADT'));
// Warn to STDERR to not interfere with STDOUT piping
console.warn(`AADTS range from ${minAADT}-${maxAADT}`);

// For each hex,
featureEach(hexFC, (hexCell) => {
  let maxHexAADT = 0;

  featureEach(aadtFC, (street) => {
    // If the street intersects the hex.
    if (boolIntersects(hexCell, street)) {
      maxHexAADT = _.max([maxHexAADT, street.properties.AADT]);
    }
  });

  // Just a shorthand
  const props = hexCell.properties;

  // Add both the raw values and the scales to the hex
  props.maxAADT = maxHexAADT;

  // Higher volume roads with higher AADTS are more dangerous so score higher for sidewalks
  // Only hexs with roads get a score
  props.AADTScore = 0;

  if (maxHexAADT) {
    props.AADTScore = scale(maxHexAADT, minAADT, maxAADT, 0, 100);
  }
});

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
