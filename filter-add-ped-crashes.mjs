/*

Given a hex grid, Read a harcoded GeoJSON file containing Points for locations
of crash data.

For each hex, count the number of pedestrian-involved and cyclist-involved crashes it contains.
  Also, count the number of crashes with injuries or fatailities

Convert this range to 100-0. That is, a score of 100 (more ped crashes)
would get the highest score for sidewalk priority.

Resulting properties added:

 - bikeCrashCnt (raw)
 - bikeCrashScore (scaled)

 - pedCrashCnt (raw)
 - pedCrashScore (scaled)

 - injuryCrashCnt (raw)
 - injuryCrashScore (scaled)

*/
import _ from 'lodash';
import fs from 'fs';
import { featureEach } from '@turf/meta';
// Takes a Point and Polygon or MultiPolygon and determines if the point resides inside the polygon.
import boolPointInPolygon from '@turf/boolean-point-in-polygon';
import { getCoords } from '@turf/invariant';
// scale(number, oldMin, oldMax, newMin, newMax);
import scale from 'scale-number-range';

// Read hex file input from STDIN. Start with hex-file-sample.geojson first!
const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

// Load the Traffic-calmed locations data.
// It's an FeatureCollection full of Points.
const crashFile = fs.readFileSync('/home/mark/Documents/Mapping/IU/master.geojson');
// FC = Feature Collection
const crashPointsFC = JSON.parse(crashFile);

// For each hex,
featureEach(hexFC, (hexCell) => {
  // shorthand
  const props = hexCell.properties;

  // reset any pre-existing value
  delete props.bikeCrashScore;
  delete props.pedCrashScore;
  delete props.injuryCrashScore;
  props.bikeCrashCnt = 0;
  props.pedCrashCnt = 0;
  props.injuryCrashCnt = 0;

  featureEach(crashPointsFC, (crashPoint) => {
    // Look at ped crashes
    if (crashPoint.properties.p) {
      // If the ped crash point is within the hex, increments the count for that hex.
      if (boolPointInPolygon(getCoords(crashPoint), hexCell)) {
        props.pedCrashCnt += 1;
      }
    }

    // Look at cyclist crashes
    if (crashPoint.properties.c) {
      // If the ped crash point is within the hex, increments the count for that hex.
      if (boolPointInPolygon(getCoords(crashPoint), hexCell)) {
        props.bikeCrashCnt += 1;
      }
    }

    // Look at injury and death crashes
    if (crashPoint.properties.i || crashPoint.properties.d) {
      // If the crash point is within the hex, increments the count for that hex.
      // No bonus points for fatalities or multiple injured people
      if (boolPointInPolygon(getCoords(crashPoint), hexCell)) {
        props.injuryCrashCnt += 1;
      }
    }
  });
});

// Calculate the minimum and maximum crash points per hex.
// A min of zero is presumed, but we confirm anyway.
const minBikeCnt = _.min(_.map(hexFC.features, 'properties.bikeCrashCnt'));
const maxBikeCnt = _.max(_.map(hexFC.features, 'properties.bikeCrashCnt'));

const minPedCnt = _.min(_.map(hexFC.features, 'properties.pedCrashCnt'));
const maxPedCnt = _.max(_.map(hexFC.features, 'properties.pedCrashCnt'));

const minInjuryCnt = _.min(_.map(hexFC.features, 'properties.injuryCrashCnt'));
const maxInjuryCnt = _.max(_.map(hexFC.features, 'properties.injuryCrashCnt'));

// For each hex, calculate the pedCrashScore
featureEach(hexFC, (hexCell) => {
  // shorthand
  const props = hexCell.properties;

  if (props.bikeCrashCnt === 0) {
    props.bikeCrashScore = 0;
  } else {
    props.bikeCrashScore = scale(props.bikeCrashCnt, minBikeCnt, maxBikeCnt, 1, 100);
  }

  if (props.pedCrashCnt === 0) {
    props.pedCrashScore = 0;
  } else {
    props.pedCrashScore = scale(props.pedCrashCnt, minPedCnt, maxPedCnt, 1, 100);
  }

  if (props.injuryCrashCnt === 0) {
    props.injuryCrashScore = 0;
  } else {
    props.injuryCrashScore = scale(props.injuryCrashCnt, minInjuryCnt, maxInjuryCnt, 1, 100);
  }
});

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
