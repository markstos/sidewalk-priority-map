/*

Given a hex grid, Read the 2018 Pavement Centerline GeoJSON file
and calculate the max street width and signed speed for each by
looking at streets which interspect each hex.

Safe to re-run because we reset the scores of each hex when wen run.

The Pavement Centerline file is a feature collection of LineStrings.

The properties we are looking for are named `AVG_WIDTH` and `speed`.

*/
import _ from 'lodash';
import fs from 'fs';
import { featureEach } from '@turf/meta';
import boolIntersects from '@turf/boolean-intersects';
// scale(number, oldMin, oldMax, newMin, newMax);
import scale from 'scale-number-range';

// Read hex file input from STDIN. Start with hex-file-sample.geojson first!
const hexFile = fs.readFileSync('/dev/stdin').toString();

const hexFC = JSON.parse(hexFile);

// Load the Pavement Centerline data. It's an FeatureCollection full of LineStrings.
const pavementFile = fs.readFileSync('BloomingtonPavementCenterline2018-exported.geojson');
// FC = Feature Collection
const pavementFC = JSON.parse(pavementFile);

// Find the min and max widths and speeds of each street.
const minWidth = _.min(_.map(pavementFC.features, 'properties.AVG_WIDTH'));
const maxWidth = _.max(_.map(pavementFC.features, 'properties.AVG_WIDTH'));
// Warn to STDERR to not interfer with STDOUT piping
console.warn(`Widths range from ${minWidth}-${maxWidth}`);

const minSpeed = _.min(_.map(pavementFC.features, 'properties.speed'));
const maxSpeed = _.max(_.map(pavementFC.features, 'properties.speed'));
// Warn to STDERR to not interfer with STDOUT piping
console.warn(`Speeds range from ${minSpeed}-${maxSpeed}`);

// For each hex,
featureEach(hexFC, (hexCell) => {
  let maxHexSpeed = 0;
  let maxHexWidth = 0;

  featureEach(pavementFC, (street) => {
    // If the street intersects the hex.
    if (boolIntersects(hexCell, street)) {
      maxHexSpeed = _.max([maxHexSpeed, street.properties.speed]);
      maxHexWidth = _.max([maxHexWidth, street.properties.AVG_WIDTH]);
    }
  });

  // Just a shorthand
  const props = hexCell.properties;

  // Add both the raw values and the scales to the hex
  props.maxStreetSpeed = maxHexSpeed;
  props.maxStreetWidth = maxHexWidth;

  // Wider and faster roads are more dangerous so score higher for sidewalks
  // Only hexs with roads get a score
  props.speedScore = 0;
  props.widthScore = 0;

  if (maxHexWidth) {
    props.speedScore = scale(maxHexSpeed, minSpeed, maxSpeed, 0, 100);
  }

  if (maxHexWidth) {
    props.widthScore = scale(maxHexWidth, minWidth, maxWidth, 0, 100);
  }
});

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
