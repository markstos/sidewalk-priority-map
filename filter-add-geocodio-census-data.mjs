/*

Example of adding Geocodio census data to a hex grid file.

This version does not apply number scaling to the data to convert them into 1-100 scores.

*/
import _ from 'lodash';
import fs from 'fs';
import center from '@turf/center'
import {getCoord} from '@turf/invariant'
import Geocodio from 'geocodio-library-node'

// You must set your API key in environment variable named GEOCODIO_API_KEY
// Ex: env GEOCODIO_API_KEY=abcde...8bc ./node script.mjs<hex-grid.gejson
const geocoder = new Geocodio(process.env.GEOCODIO_API_KEY);

const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

// TEST with just the first two array elements
const features = hexFC.features.slice(undefined, 2);

const centerCoords = getCenterCoords(features);

// Generate a parallel array
const results = await geocoder.reverse(
  centerCoords,
  ['census','acs-demographics', 'acs-economics', 'acs-housing' ],
  1)  // result per query is good enough here.

// "zip" the census data by merging parallel arrays
zipCensusData(features, results.results);


/*
Given an array of GeoJSON hexagon features,
Return a parallel array of coordinate strings in the format
used by Geocod.io.
 
 */
function getCenterCoords(features) {
  return _.map(features, feature => {
    // center as a coordinate pair
    const centerCoords = getCoord(center(feature))
    // reverse them
    return `${centerCoords[1]},${centerCoords[0]}`;
  })
}


/*
 Given a hex cell properties object and a Geocoder result for the center point,
 modify the properties by reference.
*/
function zipCensusData(features, results) {
  return _.zipWith(features, results, _zipOne)
}

/*
Given one hex feature and a parallel geocoding result, merge them
and return the result.
*/
function _zipOne(feature, res) {
  const props = feature.properties;

  // Include the coords useds for geocoding for QA
  props.centerCoordString = res.query;

  // The Geocoder can produce multiple matches. We are interested in the first one.
  const match = res.response.results[0];
  props.formattedAddress = match.formatted_address;
  props.census = match.fields.census;
  props.acs = match.fields.acs;
  return feature;
}

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
