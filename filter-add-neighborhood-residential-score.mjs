/*

Given a hex grid, Read a road centerline file that contains road topology data.
In the file, we we expect "NR" to be the code for Neighborhood Residential in a property
name "Typology"

For each hex, calculate the percentage or roads in the hex that are neighborhood residential.
Then invert the range, so a hex no residential scores 100 and all residential scores 0.

If there's no lines the score is zero-- if there are no roads, there's no need for sidewalks
there.

For each hex, add neighborhoodResidentialScore property.

Safe to re-run because we reset the scores of each hex when wen run.

The road centerline file is a feature collection of MultiLineString.

*/
import fs from 'fs';
import { featureEach, flattenEach } from '@turf/meta';
import boolIntersects from '@turf/boolean-intersects';
// Return true if first geometry is completely within the second.
import boolWithin from '@turf/boolean-within';
import center from '@turf/center';
import length from '@turf/length';
// Given a LineString and a feature, return a feature collection of line strings.
import lineSplit from '@turf/line-split';

// Read hex file input from STDIN. Start with hex-file-sample.geojson first!
const hexFile = fs.readFileSync('/dev/stdin').toString();

const hexFC = JSON.parse(hexFile);

// Load the street centerline data. It's an FeatureCollection full of MultiLineString.
const streetsFile = fs.readFileSync('shapefiles/StreetTypology.geojson');
// FC = Feature Collection
const streetsFC = JSON.parse(streetsFile);

const totalHexes = hexFC.features.length;

let currentHex = 1;

// For each hex,
featureEach(hexFC, (hexCell) => {
  console.warn(`Process hex ${(currentHex += 1) / totalHexes}`);

  // Just a shorthand
  const props = hexCell.properties;

  // Initialize every cell so it's safe to re-run on the same file.
  props.neighborhoodResidentialScore = 0;

  // Track the total length in each hex so we can calculate a percentage.
  let hexStreetsKm = 0;
  // Track the length of residential within each hex.
  let hexResidentialKm = 0;

  // iterate of the MultiLineStrings flattened to LineStrings
  flattenEach(streetsFC, (street) => {
    // if the hex completely contains the street segment
    if (boolWithin(street, hexCell)) {
      const streetLength = length(street);
      hexStreetsKm += streetLength;
      if (street.properties.Typology === 'NR') {
        hexResidentialKm += streetLength;
      }
    // If the street intersects the hex.
    } else if (boolIntersects(hexCell, street)) {
      // console.error("street intersects");
      // The street may be MultiLineString. Convert to a lineString and split
      const segmentsFC = lineSplit(street, hexCell);
      // For each segment, calculate the center and check see the center is within the hex.
      featureEach(segmentsFC, (segment) => {
        if (boolWithin(center(segment), hexCell)) {
          const segmentLength = length(segment);
          hexStreetsKm += segmentLength;
          if (street.properties.Typology === 'NR') {
            hexResidentialKm += segmentLength;
          }
        }
      });
    }
  });

  //  console.error(`Got ${hexResidentialKm} out of ${hexStreetsKm}`);

  // Avoid divide by zero. No streets means no need for sidewalks
  if (hexStreetsKm === 0) {
    props.neighborhoodResidentialScore = 0;
  } else {
    const percentResidential = hexResidentialKm / hexStreetsKm;
    // Invert the score and scale 1 to 100
    props.neighborhoodResidentialScore = (1 - percentResidential) * 100;
  }
});

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
