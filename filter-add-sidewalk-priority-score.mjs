/*

Load the hex map.

Add a `sidewalkPriorityScore` field based on other fields.

*/

import fs from 'fs';
import { featureEach } from '@turf/meta';

const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

// Formula comes from: https://bloomington.in.gov/onboard/meetingFiles/download?meetingFile_id=11366
// Page 28

featureEach(hexFC, (hexCell) => {
  // shortcut
  const props = hexCell.properties;

  // All constituent values been pre-scaled as scores from 0 to 100.
  // This calculation adds a new property ranked from 0 to 100

  // Demand and Density Data
  props.sidewalkPriorityScore = props.walkPotential * 0.25
    + props.popDensityScore * 0.25
    + props.percentWalkToWorkScore * 0.07
    + props.percentTransitToWorkScore * 0.07
    + props.vehicleCntScore * 0.06

    // Safety and Harm Reduction
    + props.speedScore * 0.10
    + props.widthScore * 0.10

    // Historically Excluded Groups
    + props.percentRenterScore * 0.03
    + props.percentBIPOCScore * 0.03
    + props.incomeScore * 0.04;

  return hexCell;
});

fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
