# Sidewalk Priority Map

A collection of scripts used to create a sidewalk priority map.

The general approach is a useful reference for calculating spatial priority where several data sets need to be corrolated that may contain a mix of data types and formats such as census data, street data, building footprint data and point data like transit stops.

In this state the project is not a turn-key solution but serves as a reference and a starting point to adapt for your own use.

Contributions to make the tools more easily re-usable as welcome.

The current state hard-codes a number of reference that are specific to the spatial data available for Bloomington, Indiana.

## Method Description

### Setup

[Node.js](https://nodejs.org/en/download/) is required to run the tools here.
You'll also need to download this project to your laptop.

Once you've got Node.js and the project downloaded, from within the project directory install the dependencies:

```
npm install
```

From there, the scripts here should be able to run.

The syntax below is for Unix CLI environments. Windows can first install the free [Windows Subsystem for Linux](https://www.lifewire.com/install-bash-on-windows-10-4101773) to be able to access such an environment.


### 1. Create a hex grid

A hex grid is the foundation of all other calculations, so the first step is to create an empty hex grid. If you are analyzing a single city, create a grid within the bounding box of the city. The same goes for a state, but be aware that it can take hours to generate an empty hex grid for a state.

Choose the size of the hex depending on the project. For sidewalks in a mid-size city, I wanted the grid to be fairly detailed, so I chose a hex side of 0.5km, or about 100 meters across the hex.

### gen-empty-hex-grid.mjs

This script uses a URL that resolves a city boundary as a GeoJSON polygon and produces a hex grid with sides of 0.5km that covers the city. As generated, each grid has no properties attached to it. Each "filter" below works by taking an pre-existing hex grid as input and then "filtering" it by adding or updating one more properties to the cells in the hex grid, which can later be visualized as a heat map.


### 2. Add additional metrics to the hex grid

#### Acquiring data

To add metrics to each hex, you'll find need to acquire some source data to process. A metric like "walk potential" is calculated using OpenStreetMap data. Our metrics like "percentage of people who walk to work" come from census data, while other valuable data may come from local government's open data portal or open records request. Here I focus on how to analyze the data once you have it.

#### Convert to GeoJSON

All the tooling here works with GeoJSON, all data sources I acquire are converted to GeoJSON if they are not already in that format.

While the most recent GeoJSON standard recommends the WGS 84 datum, Bloomington, Indiana wasn't following that standard on their open data portal, geoserver. The GeoJSON should contain this: "urn:ogc:def:crs:OGC::CRS84"

In the case of geoserver, there was a hidden trick to add this to the end of the GeoJSON query strings. Then it would return the correct projection!

```
# When geoserver returned the wrong projection of GeoJSON, I added this to fix it:
&srsName=EPSG:4326
```
You'll also have a clue your data is in the wrong projection if you try to analysis and no hex cells in your city overlap with any of the data!

#### Process

The pattern used here is that each metric is added as a Unix filter. That is, an existing GeoJSON hex grid is expected on STDIN, new calculations are made, and the updated hex grid GeoJSON is written back out to STDOUT. For example, the syntax for adding transit potential could look like this:

```bash
node filter-add-transit-potential.mjs <hex-before.geojson >hex-after.geojson
```

More powerfully, starting with script that prints a hex grid to STDOUT, several filters can be chained together using pipes to create a final results:

```
node gen-empty-hex-grid.mjs \
     | filter-add-traffic-calming.mjs \
     | filter-add-transit-potential.mjs \
     > final-hex-grid.geojson
```

But in practice, there's a chance something will crash at any stage so I usually run calculation at a time.

I have written my filters in Node.js, but since each filter is an independent script, it would work just as well to write your own filter in Python or another language.

In the GeoJSON file, what's happening is that additional GeoJSON properties are being added to the polygons that represent each hex grid. ArcGIS and other GIS tools have the same concept but may call them attributes instead of properties.

To make all the metrics play nice together in formulas, every metric is converted to a score that ranges from 0 to 100. In Node, I use a module called `scale-number-range` to help with that. So for example, the values range from 230 to 856, values in that range will be scaled to match
0-100.

Depending on the situation, the ranges may also need to be inverted, so that values towards 100 are always "better" and values towards zero are always "worse".

#### Testing Changes

Some of these calculations can be time-intenstive to run. For this reason, it can be useful to have a small hex grid to play with. For example, maybe it has only 100 cells instead of 10,000 but covers an area of town that includes a mix of streets and buildings that most metrics would apply to. Having a small text file will speed up iterative testing.

```
node filter-add-transit-potential.mjs <hex-sample.geojson >hex-sample-result.geojson
```

### 3. Calculate a final priority based on several metrics and visualize the result

To visualize a map based on this data, there are two things you can do.

First, you can filter the data. For example, you can show only the hexs that have a `sidewalkScore` of 100, where 100 means sidewalks are missing there.

Second, you can generate a heatmap based on a custom formula. This formula can be implemented as yet-another filter that adds yet-another metric to the GeoJSON. That's a good choice if you want the metric embedded in the GIS data for permancy of if you need advanced formula calculations. But for flexibility, the formula can be developed directly in a mapping application like ArcGIS Online, where several different visualizations can be made by applying different formulas to the same source data.

For example, one visualization might simply show the "walk potential" metric as a heatmap across the whole city, while a second map might show a map of missing sidewalk sections colored as a heatmap based on a formulat that calculates the primary to fill in those gaps.

ArcGIS Online calls this "Styling the Map based on an expression". Here's an example expression:

```
$feature.walkPotential*.25
+$feature.popDensityScore*.25
+$feature.percentWalkToWorkScore*.07
+$feature.percentTransitToWorkScore*.07
+$feature.vehicleCntScore*.06
+$feature.speedScore*0.10
+$feature.widthScore*0.10
+$feature.incomeScore*0.04
+$feature.percentRenterScore*0.03
+$feature.percentBIPOCScore*0.03
```

What's happening there are different weightings are being applied to a number of metrics such that all the weights all add up to 1.

This is possible because all the scores have been calculated to share the same 0-100 scale. In this way, 10 variables can be considered in the final formula, with some given more weight than others.

A human-friendly version of the same breakdown might look like this:

```
  69% Demand:
    25% walkPotential
    25% densityScore
    7% percentWalkToWorkScore
    6% percentTransitToWorkScore
    6% vehicleCntScore
  
  21% Safety:
    7% speedScore
    7% widthScore
    7% trafficCalmScore
  
  10% Equity:
    4% incomeScore
    3% percentRenterScore
    3% percentBIPOCScore
```

An example of calculating a final priority is provided in [filter-add-sidewalk-priority-score.mjs](./filter-add-sidewalk-priority-score.mjs)

## Metric calculations provided here

These are all scripts I used to calculate metrics in Bloomington, Indiana. Consider them more examples and less universal solutions.

### filter-add-btown-census-data.mjs

Here I was provided local census data that appear it may have already been given custom attribute names.

For interesting census details check to see if the hex center is within the census area polygon. If so, add scores for those census elements to the hex cell.

### filter-add-geocodio-census-data.mjs

Another source of census data is [Geocod.io](https://geocod.io).

For the centerpoint of each hex cell
  Make an API call to Geocod.io for related census data
  Add all the census data returned to the hex.

*I did not end up using this-- number scaling needs to be applied before the values return can play nice with other metrics!*

### filter-add-building-footprints.mjs

The idea here was to create a "building density" score, which may be different that "population density". For example, sidewalks are useful where are there are buildings. Compared to other metrics, this was didn't end up being that useful, but it's included here since I did the calculations.

Given a map of building footprints, calculate the percentage of each hex cell covered by building.

### filter-add-max-aadt.mjs

Given a map of road centerlines with Average Annual Daily Trip data (AADT), calculate the max AADT throughout the city.

For each hex cell
  Find all the streets that intersect the cell
  Calculate the max AADT for any intersecting street.
  `maxAADT` will contain the raw value.
  `AADTSCore` will be scaled from 0 to 100.

### filter-add-max-street-width-and-speed.mjs

Given a map of roads with their widths and signed speeds,

For each hex cell
  Calculate the max speed of any road intersecting the cell.
  Calculate the max width of any road intersecting the cell.
  Add `maxStreetSpeed` and `maxStreetwidth` properties with raw values.
  Add `speedScore`, scaled 0-100. Faster is higher.
  Add `widthScore`, scaled 0-100. Wider is higher.

### filter-add-missing-sidewalk-distance.mjs

This is an example of how to find the "Top 5 miles of sidewalks (or roads) that meet X criteria".

This is a more advanced analysis that is built on other work:

Given a hex grid that already contains "walk potential" and a [map of missing sidewalks](https://www.youtube.com/watch?v=gkKpp8nzCSA), rank all the hex cells by their walk potential to find the
top 5 and 10 miles of missing sidewalks in locations with the highest walk potential.
Sort all the hex cells by walk potential.

For each hex cell
  If the cell completely contains a missing sidewalk section, add it to the hex's missing length.
  If the missing sidewalk intersects with the cell,
    check if the center if the segment is within the hex  
    IF so, assign the lenght to that hex.
  Check if the distance of all missing segments so far exceeds 5 or 10 miles.

### filter-add-neighborhood-residential-score.mjs

Given the generally slower, lower-traffic speeds of neighborhood streets, it may despirable to prioritize sidewalk projects along busier, wider and faster roads.

`neighborhoodResidentialScore` is higher when the percentage of neighborhood residential
roads is lower.

Given a map of street topology in GeoJSON format,

For each hex cell in a hex grid
  If cell completely contains a road, calculate the total it contains.
  If the cell completely contains any neighbor roads, add track this separately.
  If the cell intersects a street
    check if the center of a street segment is within the cell.
    If cell contains a road segment, add this to total road distance in the cell.
    If cell contains a neighborhood road segment, add this to total neighborhood road distance.
  Calculate the percentage of residential roads and then invert it.

### filter-add-traffic-calming.mjs

Traffic-calming can increase the safety of walking on roads without sidewalks for about 10% of the cost. Roads that have neither sidewalks no traffic-calming may be higher priority to receive sidewalks.

Given a FeatureCollection of Points with traffic-calmed locations,

For each cell in a hex grid
  If there's a traffic-calmed location within the hex, add a traffic-calming point to the cell.

Calculate a `trafficCalmedScore` from 1 to 100 so less traffic-calming scores higher.

### filter-add-transit-potential.mjs (requires isochrone service)

Sidewalks near transit stops makes the transit service more effective, safe and welcoming. This metric was one that Blomington ended up using to help find missing sidewalks near bus stops so those connections could be prioritized.

This analysis requires a web service that can generate isochrones.

Given a FeatureCollection of points containing bus stop points,
  generation 5 and 10-minute walk isochrones for all the bus stop locations.

For each hex cell
  If the center of the cell is within the 5-minute isochrone,
    set `transitPotentialScore` = 100
  If the center of the cell is within the 10-minute isochrone,
    set `transitPotentialScore` = 50

This pattern could be re-used to check for proximity for other types of locations

### filter-hex-grid-for-density.mjs (Example for filtering and formula)

This script provides an example of using your own software to do filtering
and calculating a priority formula instead of another tool like ArcGIS online.

Here, the hex grid for a city is filtered to just those with missing or poor condition sidewalks based on a `sidewalkScore` in the hex grid.

Then the `walkPotential` and `buildingDensity` values from the input are used to calculate a final `priority` score.

After trying this approarch, I later came to prefer the approach of using ArcGIS online for these steps, where it was iteratively update the formulas and see how the visualizations change.

### filter-pavement-centerline.mjs (Example of reducing file size)

A particular road centerline file I received was bulky to deal with because it contained so many properties for each road segment.

This filter is example of how the features GeoJSON FeatureCollection can be efficiently slimmed down to just the properties you care about.

### filter-add-ped-crashes.mjs

Given:
 - Hex grid on STDIN
 - Harcoded GeoJSON file containing crash points

Calculate counts and scores from 100 for pedestrian-involved crashes, cyclist-involved crashes
and crashes involving injury or death.

Resulting properties added:

 - bikeCrashCnt (raw)
 - bikeCrashScore (scaled)

 - pedCrashCnt (raw)
 - pedCrashScore (scaled)

 - injuryCrashCnt (raw)
 - injuryCrashScore (scaled)

### filter-add-sidewalk-condition.mjs

Given:
  - Hex grid on STDIN
  - hardcoded GeoJSON file with a feature collection of sidewalk segments with condition data
  - hardcoded GeoJSON file wth a feature collection of missing sidewalk segments

Calculate a score of 0, 1 or 2 for each segment:

 - 0 for Good/Fair sidewalk locations
 - 1 for Poor sidewalk locations
 - 2 for Missing sidewalk locations

Note: Number scaling is not applied here. Considering using values of 50 and 100 to readily compose this metric with others scaled 0-100.

### Tip for writing filters: Initialize your values!

Here's an easy mistake to make when writing filters and the easy fix.

Consider a filter that sets `sidewalkScore` to 100 where are missing sidewalks. You run the filter, make a great map, then go fix some of the sidewalks.

Next year, you run the filter against a "hex grid" file that has already
has the `sidewalkScore` values from last year. Does it work?

It depends! If you only set `sidewalkScore` for cells with missing or poor condition sidewalks, now you've got a problem, there's no way to find and unset those values.

The solution is to initialize a value for every metric you set, for every cell you touch, every time you run the filter. In this case, start out by setting `sidewalkScore` to 0 for every cell, then set it to a different value if the sidewalk is detected to be missing or in poor condition there.

If you miss this step, the filter may not be accurate if you run it against a hex grid that has values previously set.

## Related Projects

 * [Walk Potential](https://gitlab.com/markstos/walk-potential) can be used to add a walk potential metric to grids generated by this tool.

## Author

 Mark Stosberg <mark@stosberg.com>
