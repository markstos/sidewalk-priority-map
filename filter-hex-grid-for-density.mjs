import _ from 'lodash';
import fs from 'fs';
import scale from 'scale-number-range';

const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

const beforeCnt = hexFC.features.length;

console.warn(`Filtering input with ${beforeCnt} features to only those where sidewalkScore > 0`);

const filteredFeatures = hexFC.features.filter((feature) => feature.properties.sidewalkScore > 0);

hexFC.features = filteredFeatures;

/* eslint-disable no-param-reassign */
// Correct typo in property name.
hexFC.features.forEach((feature) => {
  feature.properties.buildingDensity = feature.properties.buidingDensity;
  delete feature.properties.buidingDensity;
});

const maxDensity = _.max(_.map(filteredFeatures, 'properties.buildingDensity'));
console.warn(`max density is ${maxDensity}`);

const maxPotential = _.max(_.map(filteredFeatures, 'properties.walkPotential'));
console.warn(`max density is ${maxPotential}`);

// scale all the values to be 1-100
hexFC.features.forEach((feat) => {
  feat.properties.buildingDensity = scale(feat.properties.buildingDensity, 0, maxDensity, 0, 100);
  feat.properties.sidewalkScore = scale(feat.properties.sidewalkScore, 0, 2, 0, 100);
  feat.properties.walkPotential = scale(feat.properties.walkPotential, 0, maxPotential, 0, 100);

  // Scale final score down to 1-100 and round to 3 digits
  feat.properties.priority = _.round(
    (feat.properties.sidewalkScore * 2
       + feat.properties.walkPotential
       + feat.properties.buildingDensity) / 4,
    3,
  );
});

const afterCnt = filteredFeatures.length;

console.warn(`Filtered to ${afterCnt} features. Done`);

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
