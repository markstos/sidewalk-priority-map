/*
Example of a simple count of hexes that match a particular criteria.

This could also be done fairly easily in QGIS.
*/
import fs from 'fs';

const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

const missing = hexFC.features.filter((cell) => cell.properties.sidewalkScore === 100);
console.log(`Got before ${hexFC.features.length} and after ${missing.length}`);
