/*
Example of generating a CSV that contains top 100 locations ranked by priority

Expects a GeoJSON file containing a Feature Collection where each feature has a `priority` property.

 */
import _ from 'lodash';
import fs from 'fs';
import center from '@turf/center';
import { getCoord } from '@turf/invariant';

const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

//
const features = hexFC.features
  .sort((first, second) => first.properties.priority - second.properties.priority)
  .slice(-100);

/*
Given an array of GeoJSON hexagon features,
Return a parallel array of coordinate strings in the format
used by Geocod.io.

 */
function getCenterCoords(feats) {
  return _.map(feats, (feature) => {
    // center as a coordinate pair
    const centerCoords = getCoord(center(feature));
    // reverse them
    return `${feature.properties.priority},${centerCoords[1]},${centerCoords[0]}`;
  });
}

const centerCoords = getCenterCoords(features);
const csvData = `priority,lat,lon\n${centerCoords.join('\n')}`;

// Write CSV to STDOUT
fs.writeFileSync('/dev/stdout', csvData);
process.exit();
