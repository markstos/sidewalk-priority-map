/*
Given:
  - Hex grid on STDIN
  - hardcoded GeoJSON file with a feature collection of sidewalk segments with condition data
  - hardcoded GeoJSON file wth  a feature collection of missing sidewalk segments

Calculate a score of 0, 50 or 100 for each segment:

 - 0 for Good/Fair sidewalk locations
 - 50 for Poor sidewalk locations
 - 100 for Missing sidewalk locations
*/

import _ from 'lodash';
import path from 'path';
import fs from 'fs';

import boolIntersects from '@turf/boolean-intersects';
import { featureEach } from '@turf/meta';

const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

let conditionFile;
try {
  conditionFile = fs.readFileSync(path.join(
    '../sidewalk-priorities-map',
    '2021-07-16-sidewalk-conditions.geojson',
  ));
} catch (err) {
  console.error('Reading failed.');
  console.error('Error was: ', err);
}

// This is a GeoJSON Feature Collection where each sidewalk segment is a feature.
// We will be looking at the Condition_ Property for values of Good, Fair or Poor
const conditionFeatures = JSON.parse(conditionFile);

const conditionScore = {
  Good: 0, // Good and Fair sidewalks aren't prioritized
  Fair: 0,
  Poor: 50,
  Missing: 100,
};

let missingFile;
try {
  // The City should provide a cleaned up, official missing sidewalk map.
  missingFile = fs.readFileSync(path.join(
    '../sidewalk-priorities-map',
    '2020-bloomington-indiana-city-maintained-streets-with-no-sidewalks-lines.geojson',
  ));
} catch (err) {
  console.error('Reading failed.');
  console.error('Error was: ', err);
}

// This is a GeoJSON Feature Collection where each sidewalk segment is a feature.
// We will be looking at the Condition_ Property for values of Good, Fair or Poor
const missingFeatures = JSON.parse(missingFile);

const totalCells = hexFC.features.length;
let curCell = 0;

hexFC.features.forEach((cell) => {
  curCell += 1;
  console.warn(`Calculating cell ${curCell}/${totalCells} `);

  cell.properties.sidewalkScore = 0;

  featureEach(missingFeatures, (feature) => {
    // If this cell has already gotten score, skip the rest of the checks.
    if (_.get(cell, 'properties.sidewalkScore') > 0) {
      return;
    }

    // TODO: When I get a fixed "missing sidewalk" file, I should check for "contains", too.
    // if (boolIntersects(cell, feature) || boolContains(cell, feature)) {
    if (boolIntersects(cell, feature)) {
      _.set(cell, 'properties.sidewalkScore', conditionScore.Missing);
    }
  });

  // Score an additional point for this cell for each amenity layer that contains it.
  featureEach(conditionFeatures, (feature) => {
    // If this cell has already gotten score, skip the rest of the checks.
    if (cell.properties.sidewalkScore > 0) {
      return;
    }

    // If the cell intersects any Poor sidewalks
    // TODO: I would like to also check for "contains" any poor sidewalks,
    // but first I have to convert the lines to polygons, I thin || boolContains(cell, feature)
    /* eslint-disable no-underscore-dangle */
    if (feature.properties.Condition_ === 'Poor'
      && boolIntersects(cell, feature)) {
      _.set(cell, 'properties.supply.sidewalkScore', conditionScore.Poor);
    } else if (feature.properties.Condition_ === 'Fair'
      && boolIntersects(cell, feature)) {
      _.set(cell, 'properties.supply.sidewalkScore', conditionScore.Fair);
    } else if (feature.properties.Condition_ === 'Good'
      && boolIntersects(cell, feature)) {
      _.set(cell, 'properties.supply.sidewalkScore', conditionScore.Good);
    }
  });
});

// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
