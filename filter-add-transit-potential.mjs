/*
 
Given a hex grid, read a hardcoded GeoJSON file containing Points
locations of Bloomington Transit stops.

For each bus stop location, generate 10 minute walk isochrone.

Reduce/union all isocrones to create a single isochrone for the whole network.

For each hex in the each grid:

  - Find the center.
  - set score to 100 if the center is within the isochrone, zero if it isn't.

Requires setting OPEN_TRIP_PLANNER_ROUTER_URL environment variable.
Ex: env OPEN_TRIP_PLANNER_ROUTER_URL=https://company.com/otp/routers/mine node ./script.mjs

*/

import Promise from 'bluebird'
import fs from 'fs';
import { featureCollection } from '@turf/helpers';
import { featureEach } from '@turf/meta';
import center from '@turf/center'
import truncate from '@turf/truncate'
import fetch from 'node-fetch'
import union from '@turf/union'
import boolWithin from '@turf/boolean-within';

// How many concurrent requests can your OpenTripPlanner server handle?
const CONCURRENCY = 16;

// Read hex file input from STDIN. Start with hex-file-sample.geojson first!
const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

// Load Bloomington Transit stop location data. 
// It's an FeatureCollection full of Points.
const stopsFile = fs.readFileSync('shapefiles/BTFallRidershipKML.geojson');
// FC = Feature Collection
const stopsFC = JSON.parse(stopsFile);

// Important to log to STDERR, since STDOUT will be redirected to a file.
console.warn(`Starting to generate isochrones for ${stopsFC.features.length} bus stops`);

// Keep track of our progres generating isochrones
var i = 1;

// For all stops, generate the 10 minute isochrone intermediate result
const multiPolygonTenMinFeatures = await Promise.map(
  stopsFC.features, 
  isoChroneForOneFeature.bind(undefined, 10), 
  { concurrency: CONCURRENCY });

// Show our work by writing out 10-minute isochrone
fs.writeFileSync( 'shapefiles/BT-10min-isochrone.geojson', 
  JSON.stringify(featureCollection(multiPolygonTenMinFeatures)))

console.warn("Calculating union of 10-minute isochrones");

// Union all the isochrones on the layer together and return;
const transitTenMinIsochrone = multiPolygonTenMinFeatures.reduce(
  (acc, feature) => union(truncate(acc), truncate(feature)))

// For all stops, generate the 5 minute isochrone
const multiPolygonFiveMinFeatures = await Promise.map(
  stopsFC.features, 
  isoChroneForOneFeature.bind(undefined, 5), 
  { concurrency: CONCURRENCY });

// Show our work by writing out 5-minute isochrone intermediate result
fs.writeFileSync( 'shapefiles/BT-5min-isochrone.geojson', 
  JSON.stringify(featureCollection(multiPolygonFiveMinFeatures)))

console.warn("Calculating union of 5-minute isochrones");
//
// Union all the isochrones on the layer together and return;
const transitFiveMinIsochrone = multiPolygonFiveMinFeatures.reduce(
  (acc, feature) => union(truncate(acc), truncate(feature)))


// For each hex,
featureEach(hexFC, hexCell => {

  // shorthand
  const props = hexCell.properties;

  const cellCenter = center(hexCell);

  // reset any pre-existing detected transit potential.
  delete props.transitPotentialScore;
  props.transitPotentialScore = 0;

  // If the location is with the isochrones score it.
  // Being within 5 minutes of a stop is worth twice as much as 10;
  if (boolWithin(cellCenter, transitFiveMinIsochrone)) {
    props.transitPotentialScore = 100
  }
  else if (boolWithin(cellCenter, transitTenMinIsochrone)) {
    props.transitPotentialScore = 50;
  }
})


async function isoChroneForOneFeature (minutes, feature) {
  // Whether's a point or polygon, find the center
  // Return value is a Feature of Type Point
  let centerPoint = center(feature)
  const [lon, lat] = centerPoint.geometry.coordinates;

  console.warn(`Starting ${i++}/${stopsFC.features.length} isochrone`);
  return getIsoChrone(lon, lat, minutes)
}

// get a lon, lat and minutes, return GeoJSON Feature <MultiPolygon> representing isochrone for that point.
async function getIsoChrone(lon, lat, minutes) {
  const seconds = minutes*60;

  // Currently a 10 Minute walk isochrome is hardcoded.
  const url = `${process.env.OPEN_TRIP_PLANNER_ROUTER_URL}/isochrone?fromPlace=${lat},${lon}&mode=WALK&date=07-06-2021&time=8:00am&cutoffSec=${seconds}`
  // Expect a single multiPolygon is always returned with a FeatureCollection
  const response = await fetch(url);
  const featureCollection = await response.json()
  return featureCollection.features[0];
}



// Write hex GeoJSON to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
