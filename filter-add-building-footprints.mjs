/*
 
 Load the hex map.
 Load the building frontprint map.

  For each hex, calculate the intersection with the building foortprint map.

  The result is the buildings within the hex.

  Next, calculate the area in hectacres (square kilometers) of the buildings with the hex.

  Finally, divide the area by 25.981 -- the number of square km in a 100 meter hexagon. 

  This is the percentage of the hexagon covered by buildngs.
*/

import Promise from 'bluebird';
import fs from 'fs';
import area from '@turf/area';
import intersect from '@turf/intersect';

const hexFile = fs.readFileSync('/dev/stdin').toString();

// This is a GeoJSON Feature Collection where each sidewalk segment is a feature.
// We will be looking at the Condition_ Property for values of Good, Fair or Poor
const hexFC = JSON.parse(hexFile);

let footprintFile;
try {
  footprintFile = fs.readFileSync('footprints-combined.geojson')
}
catch (err) {
  console.error(`Reading failed.`)
  console.error("Error was: ", err)
}

// This is a GeoJSON Feature Collection full of multi-polygons
const footprintMultiPoly = JSON.parse(footprintFile).features[0];

const totalCells = hexFC.features.length;
let curCell = 0;

//hexFC.features.forEach(cell => {
hexFC.features = await Promise.map(hexFC.features, async (cell) => {
  curCell++
  console.log(`${curCell}/${totalCells} calculating buildingDensity`)
  
  // For each hex, calculate the intersection with the building foortprint map.
  // The result is the buildings within the hex.
  const intersection = intersect(cell, footprintMultiPoly);
  // Next, calculate the area in square meters of the buildings with the hex.
  // Finally, divide the area by 25981 -- the number of square meters in a 100 meter-side hexagon. 
  // intersection may be null 
  if (intersection) {
    cell.properties.buidingDensity = area(intersection) / 25981;
  }
  else {
    cell.properties.buidingDensity = 0
  }

  return cell;
}, { concurrency: 5 })

fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
