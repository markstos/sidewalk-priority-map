/*

Given a hex grid, calculate census values from the custom-format
census GeoJSON provided by the city.

 */
import _ from 'lodash';
import fs from 'fs';
import center from '@turf/center';
import { featureEach } from '@turf/meta';
import boolWithin from '@turf/boolean-within';
// scale(number, oldMin, oldMax, newMin, newMax);
import scale from 'scale-number-range';

// Read hex file input from STDIN. Start with hex-file-sample.geojson first!
const hexFile = fs.readFileSync('/dev/stdin').toString();
const hexFC = JSON.parse(hexFile);

// Load the Census data. It's an FeatureCollection full of polygons.
const censusFile = fs.readFileSync('private/geojson/Bloomington_Sidewalk_Priority_Census_Data.geojson');
const censusFC = JSON.parse(censusFile);

// Higher is better, except those marked with "reverse"
// The output percentages are truly percentages anymore because they are scaled
// relative to the min and max of the input range.
const fieldsToKeep = {
  PerBIPOC: { newName: 'percentBIPOCScore' },
  MedHHInc19: { newName: 'incomeScore', reverse: true },
  HHMedVeh19: { newName: 'vehicleCntScore', reverse: true },
  PopDen19mi: { newName: 'popDensityScore' },
  PerRenter: { newName: 'percentRenterScore' },
  PerWalkWrk: { newName: 'percentWalkToWorkScore' },
  PerTranstW: { newName: 'percentTransitToWorkScore' },
};

// Calculate the min and max for each census value
Object.keys(fieldsToKeep).forEach((key) => {
  fieldsToKeep[key].min = _.min(_.map(censusFC.features, `properties.${key}`));
  fieldsToKeep[key].max = _.max(_.map(censusFC.features, `properties.${key}`));
  // Use warn to get to STDERR to not interfere with STDOUT redirection
  console.warn(`Values for ${key} range from ${fieldsToKeep[key].min}-${fieldsToKeep[key].max}`);
});

/// ///////////////////

/*
Given a target object and a source object, modify the target object
by reference by adding the `fieldsToKeep` from the source.

Target fields names will be based on `fieldsToKeep.newName`.

Target values will be scaled from 1-100 using the min and max values
of the inputs as the input range.

If `fieldsToKeep.reverse` is given, target values will be reversed
in the the range, so that lower inputs will score higher in the output range.
*/
function addProps(target, source) {
  /* eslint-disable no-param-reassign */
  Object.keys(fieldsToKeep).forEach((key) => {
    // Reverse the scaling
    if (fieldsToKeep[key].reverse) {
      target[fieldsToKeep[key].newName] = scale(
        source[key],
        fieldsToKeep[key].min, // oldMin,
        fieldsToKeep[key].max, // oldMax,
        100,
        0, // newMin, newMax -- reverse scaling!
      );
    } else { // Normal scaling
      target[fieldsToKeep[key].newName] = scale(
        source[key],
        fieldsToKeep[key].min, // oldMin,
        fieldsToKeep[key].max, // oldMax,
        0,
        100, // newMin, newMax
      );
    }
  });
}

// For each hex,
featureEach(hexFC, (hexCell) => {
  // Calculate the center point.
  // Loop over each polygon of census data.
  featureEach(censusFC, (censusPolygon) => {
    // If the hex center is within the polygon,
    if (boolWithin(center(hexCell), censusPolygon)) {
      //  Add the polygon's attributes to the hex
      addProps(hexCell.properties, censusPolygon.properties);
    }
  });
});

// Write output to STDOUT
fs.writeFileSync('/dev/stdout', JSON.stringify(hexFC));
process.exit();
