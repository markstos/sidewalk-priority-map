/*

Given a GeoJSON boundary for city, generate a GeoJSON hex grid for the city and print it to STDOUT.

Requires setting CITY_BOUNDARY_GEOJSON_URL in the environment.

Ex: env CITY_BOUNDARY_GEOJSON_URL='https://bloomington.in.gov/geoserver/publicgis/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=publicgis:BloomingtonMunicipalBoundary&maxFeatures=50&outputFormat=application%2Fjson&srsName=EPSG:4326' node ./gen-empty-hex-grid.mjs >bloomington-hex-grid.geojson


*/
import bbox from '@turf/bbox';
import combine from '@turf/combine';
import fetch from 'node-fetch';
import fs from 'fs';
import hexGrid from '@turf/hex-grid';

// The projection must be EPSG:4326!
// The result is lossy in that it fills in donut holes, but it's OK for this use.
async function getCityBoundaryMultiPolygon () {
  const url = process.env.CITY_BOUNDARY_GEOJSON_URL;
  const response = await fetch(url);
  const featureCollection = await response.json()
  const simplified = combine(featureCollection);
  return simplified.features[0];
}

/* Return a Hex Grid that covers a city and is masked to it.
 * This is useful to create a heatmap.
 * "Hexagons are preferable when your analysis includes aspects of connectivity or movement paths."
 * Ref: https://pro.arcgis.com/en/pro-app/2.7/tool-reference/spatial-statistics/h-whyhexagons.htm
 * Each cell is the width of 1/3 of block, which are about 300 meters on the short side.
 * Because we providing the radius, a cellSide value of 50 meters is used, or 0.05 kilometers
 * Ref: http://turfjs.org/docs/#hexGrid
 */
function getHexGrid (mask, bbox) {
  var cellSide = 0.05;
  var options = {
    units: 'kilometers',
    mask: mask,
    properties: {
    }
  };
  return hexGrid(bbox, cellSide, options);
}


console.warn("Building hex grid. Takes a while.");

const cityMultiPolygon = await getCityBoundaryMultiPolygon();
const cityBbox = bbox(cityMultiPolygon);
const cityGrid = getHexGrid(cityMultiPolygon, cityBbox);

fs.writeFileSync('/dev/stdout', JSON.stringify(cityGrid));

